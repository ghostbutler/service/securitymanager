#!/bin/bash

# check
if [ "$EUID" -ne 0 ]; then
    echo "you are not running script as root, exiting"
    exit 1
fi

# ONLY LINE TO CHANGE HERE
# service (no spaces)
SERVICE=securitymanager

# notify
echo "installing ${SERVICE}..."

# stopping service
echo "stopping service ${SERVICE}"
systemctl stop ${SERVICE} 2>/dev/null

# create directories
echo "creating ghost directory"
mkdir $HOME/ghost 2>/dev/null
mkdir /etc/ghost 2>/dev/null
mkdir /etc/ghost/bin 2>/dev/null
mkdir /etc/ghost/conf 2>/dev/null

# copy executable
echo "copy executable to ghost directory"
go build -o bin/${SERVICE}
cp bin/${SERVICE} /etc/ghost/bin/${SERVICE}
rm bin/${SERVICE}

# copy configuration
cat configuration.example > /etc/ghost/conf/${SERVICE}.conf

# copy service file
echo "copy service file"
cp ${SERVICE}.service /etc/systemd/system/${SERVICE}.service

# reload daemon
echo "reload daemon"
systemctl daemon-reload

# enable service
echo "enable service"
systemctl enable ${SERVICE}

# start service
echo "start service"
systemctl restart ${SERVICE}

# display service status
systemctl status ${SERVICE}
