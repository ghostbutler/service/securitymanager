package service

import (
	"fmt"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/redis"
)

const (
	VersionMajor = 1
	VersionMinor = 0
)

type Service struct {
	// service
	service *common.Service

	// configuration
	configuration *Configuration

	// redis handler
	redisHandler *redis.Handler
}

// build service
func BuildService(listeningPort int,
	configuration *Configuration) (*Service, error) {
	// notify
	fmt.Println("security manager is booting")

	// allocate service
	service := &Service{
		configuration: configuration,
	}

	// build redis handler
	if redisHandler, err := redis.BuildHandler(configuration.Redis.Hostname,
		configuration.Redis.Password); err == nil {
		service.redisHandler = redisHandler
	} else {
		return nil, err
	}

	// build service
	service.service = common.BuildService(listeningPort,
		common.ServiceSecurityManager,
		SecurityManagerAPIService,
		VersionMajor,
		VersionMinor,
		nil,
		nil,
		"127.0.0.1",
		service)

	// notify
	fmt.Println("security manager has boot up, listening on port",
		listeningPort)
	fmt.Println("connecting to redis at",
		configuration.Redis.Hostname)

	// ok
	return service, nil
}

// close service
func (service *Service) Close() {
}
