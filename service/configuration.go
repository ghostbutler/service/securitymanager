package service

import (
	"encoding/json"
	"io/ioutil"
)

type AuthenticationEntry struct {
	// level name
	Level string `json:"level"`

	// basic http auth
	Basic struct {
		// login
		Login string `json:"login"`

		// sha512 password hash
		PasswordHash string `json:"password"`
	}
}

type Configuration struct {
	// store key length
	StoreKeyLength int `json:"storeKeyLength"`

	// authentication data
	Authentication struct {
		// access
		Access []AuthenticationEntry `json:"access"`
	} `json:"authentication"`

	// redis configuration
	Redis struct {
		// hostname
		Hostname []string `json:"hostname"`

		// authentication password
		Password string `json:"password"`
	} `json:"redis"`
}

// load configuration
func BuildConfiguration(filePath string) (*Configuration, error) {
	// read configuration file
	if content, err := ioutil.ReadFile(filePath); err == nil {
		// try to parse
		var configuration Configuration
		if err := json.Unmarshal(content, &configuration); err == nil {
			return &configuration,
				nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}
