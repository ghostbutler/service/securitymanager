package service

import (
	"errors"
	"fmt"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"gitlab.com/ghostbutler/tool/service/redis"
	"time"
)

const (
	RedisLockKey = "lock:authentication"
	RedisKey     = "authentication:"
)

// find authentication entry
// returns the authentication if found or nil
func (service *Service) findAuthentication(login string,
	password string) *AuthenticationEntry {
	// iterate known authentications
	for _, authentication := range service.configuration.Authentication.Access {
		if authentication.Basic.Login == login &&
			authentication.Basic.PasswordHash == password {
			return &authentication
		}
	}

	// not found
	return nil
}

// generate an entry and add it to store
func (service *Service) generateRandomEntry(serviceName string,
	serviceIP string,
	_type keystore.Type,
	level string) *keystore.Key {
	// build key
	key := keystore.BuildKey(serviceName,
		serviceIP,
		_type,
		keystore.GenerateRandomKey(service.configuration.StoreKeyLength),
		keystore.GenerateRandomKey(service.configuration.StoreKeyLength),
		level)

	// ok
	return key
}

// generate new api key (test if already exists)
// redis lock must be obtained
func (service *Service) generatePublicAPIKey(serviceName string,
	serviceIP string,
	_type keystore.Type,
	level string) (*keystore.Key, error) {
	for {
		key := service.generateRandomEntry(serviceName,
			serviceIP,
			_type,
			level)
		if result := service.redisHandler.Client.Exists(RedisKey +
			key.PublicKey); result.Err() == nil {
			if result.Val() == 0 {
				return key, nil
			} else {
				continue
			}
		} else {
			return nil, result.Err()
		}
	}
}

// push new authentication entry to redis
func (service *Service) pushKey(key *keystore.Key) error {
	// key name
	keyName := RedisKey +
		key.PublicKey

	// push fields
	if err := service.redisHandler.Client.HSet(keyName,
		"level",
		key.Level).Err(); err != nil {
		return err
	}
	if err := service.redisHandler.Client.HSet(keyName,
		"type",
		keystore.KeyTypeName[key.Type]).Err(); err != nil {
		return err
	}
	if err := service.redisHandler.Client.HSet(keyName,
		"private",
		key.PrivateKey).Err(); err != nil {
		return err
	}
	if err := service.redisHandler.Client.HSet(keyName,
		"serviceName",
		key.ServiceName).Err(); err != nil {
		return err
	}
	if err := service.redisHandler.Client.HSet(keyName,
		"serviceIP",
		key.ServiceIP).Err(); err != nil {
		return err
	}
	if err := service.redisHandler.Client.HSet(keyName,
		"creationDate",
		time.Now().Format(time.RFC3339)).Err(); err != nil {
		return err
	}

	// we want the key to expire
	if err := service.redisHandler.Client.Expire(keyName,
		keystore.KeyDuration[key.Type]).Err(); err != nil {
		return err
	}

	// no error
	return nil
}

// pull key
// redis lock must be obtained
func (service *Service) pullKey(publicKey string) (*keystore.Key, error) {
	// get key
	keyName := RedisKey +
		publicKey
	if result := service.redisHandler.Client.HGetAll(keyName); result.Err() == nil {
		if _type, ok := keystore.KeyNameType[result.Val()["type"]]; ok {
			key := &keystore.Key{
				ServiceIP:   result.Val()["serviceIP"],
				ServiceName: result.Val()["serviceName"],
				PrivateKey:  result.Val()["private"],
				PublicKey:   publicKey,
				Type:        _type,
				Level:       result.Val()["level"],
			}
			if key.Type == keystore.KeyTypeOnce {
				fmt.Println("just consumed one time key delivered to",
					key.ServiceName)
				service.redisHandler.Client.Del(keyName)
			}
			return key, nil
		} else {
			return nil, errors.New("bad key")
		}
	} else {
		return nil, result.Err()
	}
}

// authenticate the service
// return an API key, if error is different of nil, API key is not delivered
func (service *Service) authenticate(login string,
	password string,
	serviceName string,
	serviceIP string) (*keystore.Key, error) {
	// find authentication entry
	if authenticationEntry := service.findAuthentication(login,
		password); authenticationEntry != nil {
		// lock redis
		if lock, err := service.redisHandler.ObtainLock(RedisLockKey); err == nil {
			defer redis.Unlock(lock)
			// find free authentication key
			if key, err := service.generatePublicAPIKey(serviceName,
				serviceIP,
				keystore.KeyTypeAPI,
				authenticationEntry.Level); err == nil {
				// push key to redis
				if err := service.pushKey(key); err == nil {
					return key, nil
				} else {
					return nil, err
				}
			} else {
				return nil, err
			}
		} else {
			return nil, errors.New("can't get redis lock")
		}
	} else {
		return nil, errors.New("authentication failed")
	}
}

func (service *Service) requestOneTimeKey(publicKey,
	privateKey string) (*keystore.Key, error) {
	// lock redis
	if lock, err := service.redisHandler.ObtainLock(RedisLockKey); err == nil {
		// unlock
		defer redis.Unlock(lock)

		// find normal key
		if key, err := service.pullKey(publicKey); err == nil {
			// request key
			if key.Type == keystore.KeyTypeAPI {
				// generate new key
				if generatedKey, err := service.generatePublicAPIKey(key.ServiceName,
					key.ServiceIP,
					keystore.KeyTypeOnce,
					key.Level); err == nil {
					// push key
					if err := service.pushKey(generatedKey); err == nil {
						return generatedKey, nil
					} else {
						return nil, err
					}
				} else {
					return nil, err
				}
			} else {
				return nil, errors.New("bad api key type for one time key request")
			}
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}

func (service *Service) findKey(publicKey string) *keystore.Key {
	if lock, err := service.redisHandler.ObtainLock(RedisLockKey); err == nil {
		defer redis.Unlock(lock)
		if key, err := service.pullKey(publicKey); err == nil {
			return key
		} else {
			fmt.Println("find key:",
				err)
			return nil
		}
	} else {
		fmt.Println("find key:",
			err)
		return nil
	}
}
