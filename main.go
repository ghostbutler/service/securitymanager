package main

import (
	"./service"
	"fmt"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/signal"
	"os"
	"time"
)

const (
	DefaultConfigurationFilePath = "conf.json"
)

// GhostButlerProject2018*

func main() {
	// is running?
	isRunning := true

	// configuration file path
	configurationFilePath := DefaultConfigurationFilePath
	if len(os.Args) > 1 {
		configurationFilePath = os.Args[1]
	}

	// load configuration
	if configuration, err := service.BuildConfiguration(configurationFilePath); err != nil {
		fmt.Println("couldn't load configuration:",
			err.Error())
	} else {
		// build service
		if srv, err := service.BuildService(common.GhostService[common.ServiceSecurityManager].DefaultPort,
			configuration); err == nil {
			// listen for signal
			signal.Listen(&isRunning)

			// wait for service
			for isRunning {
				time.Sleep(time.Second)
			}

			// close service
			srv.Close()

			// exit
			os.Exit(0)
		} else {
			fmt.Println(err)
			os.Exit(1)
		}
	}
}
